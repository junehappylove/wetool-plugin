package org.code4everything.wetool.plugin.qiniu.controller;

import org.code4everything.wetool.plugin.support.BaseViewController;

import java.io.File;
import java.util.List;

/**
 * @author pantao
 * @since 2019/8/27
 */
public class BaseQiniuController implements BaseViewController {

    @Override
    public void openMultiFiles(List<File> files) {

    }

    @Override
    public void openFile(File file) {

    }
}
